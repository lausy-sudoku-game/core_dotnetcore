# Info for developing

## dotnet core 3 and other

- [info about dotnet core 3 preview](https://docs.microsoft.com/en-us/dotnet/core/whats-new/dotnet-core-3-0)

- [info about each preview dotnwt core 3 on github](https://github.com/dotnet/core/tree/master/release-notes/3.0/preview)

- [info about c# 8.0 preview](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8)


## git managing

`git fetch -prune` - remove old 'stale' remote branches. [see stackoverflow](https://stackoverflow.com/a/44129766/6256541)
