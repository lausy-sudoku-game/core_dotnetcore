# core_dotnetcore

core of lausy sudoku game on dotnet core

[![mit license](https://img.shields.io/github/license/lausysudoku/core_dotnetcore?style=for-the-badge)](https://gitlab.com/lausy-sudoku-game/core_dotnetcore/blob/master/licence)
[![dotnet core 3 preview 8](https://img.shields.io/badge/dotnet%20core-3%20preview%208-blue.svg?longCache=true&style=for-the-badge)](https://github.com/dotnet/core/blob/master/release-notes/3.0/preview/3.0.0-preview8.md)

[![codacy code quality grade](https://img.shields.io/codacy/grade/3b0075c9175e4abd970b429ea7613841?style=for-the-badge)](https://app.codacy.com/app/allan-walpy/core_dotnetcore)
[![codefactor code quality grade](https://img.shields.io/codefactor/grade/github/lausysudoku/core_dotnetcore?style=for-the-badge)](https://www.codefactor.io/repository/github/lausysudoku/core_dotnetcore)
[![gitlab ci pipeline status](https://img.shields.io/gitlab/pipeline/lausy-sudoku-game/core_dotnetcore/master?style=for-the-badge)](https://gitlab.com/lausy-sudoku-game/core_dotnetcore/-/jobs)

[![contributor covenant v1.4 adopted](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4s?style=for-the-badge)](./docs/development/code-of-conduct.md)
