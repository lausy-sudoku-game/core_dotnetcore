using System;
using System.Collections.Generic;

namespace LausySudokuGame.Core.Common.Tree
{
    /// <summary>
    /// Represent tree node adress
    /// </summary>
    /// <value>LausySudokuGame.Core.Common.Tree.IAdress</value>
    public interface IAdress
        : IEquatable<IAdress>, IEquatable<string>, IComparable<IAdress>
    {
        /// <summary>
        /// Separation of node names
        /// </summary>
        /// <value>.</value>
        string Separator { get; }

        /// <summary>
        /// List of nodes' names from top to bottom
        /// </summary>
        /// <value>[ "Andy", "Bob", "Clyde" ]</value>
        /// <seealso cref="Full" />
        IReadOnlyList<string> Node { get; }

        /// <summary>
        /// String form of node's names list from top to bottom
        /// </summary>
        /// <value>Andy.Bob.Clyde</value>
        /// <seealso cref="Node" />
        string Full { get; }

        /// <summary>
        /// Count of nodes
        /// </summary>
        /// <value>3</value>
        int Deep { get; }

        /// <summary>
        /// Indicate if current adress points to most upper node i.e. one of first nodes
        /// </summary>
        /// <value>false</value>
        bool IsRoot { get; }

        /// <summary>
        /// Returns node one level upper
        /// </summary>
        /// <example>Andy.Bob</example>
        /// <seealso cref="GetUpper(int)" />
        IAdress GetUpper();

        /// <summary>
        /// Returns node <paramref name="count" /> level upper
        /// </summary>
        /// <param name="count">node level count</param>
        /// <example>Andy</example>
        /// <seealso cref="GetUpper()" />
        IAdress GetUpper(int count);

        /// <summary>
        /// Returns node one level lower
        /// </summary>
        /// <param name="nodeName">lower node name</param>
        /// <example>Andy.Bob.Clyde.Denis</example>
        /// <seealso cref="GetLower(IList{string})" />
        IAdress GetLower(string nodeName);

        /// <summary>
        /// Returns node <c><paramref name="nodeNames" />.Count</c> level lower
        /// </summary>
        /// <param name="nodeNames">lower nodes' names</param>
        /// <example>Andy.Bob.Clyde.Denis.Enigma</example>
        /// <seealso cref="GetLower(string)" />
        IAdress GetLower(IList<string> nodeNames);
    }
}