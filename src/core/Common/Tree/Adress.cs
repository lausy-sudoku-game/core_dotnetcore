using System;
using System.Collections.Generic;
using System.Linq;

using LausySudokuGame.Core.Common.Extensions;

namespace LausySudokuGame.Core.Common.Tree
{
    public class Adress : IAdress
    {
        public const string DefaultSeparator = ".";

        private List<string> _node { get; set; }
        public string Separator { get; private set; }

        public Adress(IList<string> node, string separator = DefaultSeparator)
        {
            if (node == null || node.Count == 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(node),
                    node,
                    "list of nodes must contain at least one item"
                );
            }

            this.Separator = separator;

            if (!this.IsCorrectNodeNames(node))
            {
                throw new ArgumentException(
                    $"node name cannot contain separator '{this.Separator}'",
                    nameof(node));
            }

            this._node = node.Clone().ToList();
        }

        public Adress(string adressString, string separator = DefaultSeparator)
        {
            if (adressString == null)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(adressString),
                    adressString,
                    "node adress cannot be null"
                );
            }

            this.Separator = separator;
            this._node = adressString.Split(this.Separator).ToList();
        }

        public int Deep => this._node.Count;
        public bool IsRoot => this.Deep == 1;
        public IReadOnlyList<string> Node => this._node.AsReadOnly();
        public string Full => String.Join(this.Separator, this._node);

        protected internal bool IsCorrectNodeName(string nodeName)
            => !nodeName.Contains(this.Separator);

        protected internal bool IsCorrectNodeNames(IList<string> nodeNames)
            => nodeNames.All(this.IsCorrectNodeName);

        public IAdress GetUpper(int count)
        {
            if (count >= this.Deep || count < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(count),
                    count,
                    "level counts must be less then adress deep and not negative"
                );
            }

            IList<string> upperNode = this._node.Clone();

            upperNode.RemoveCount(count);

            return new Adress(upperNode, this.Separator);
        }

        public IAdress GetUpper()
            => this.GetUpper(1);

        public IAdress GetLower(IList<string> nodeNames)
        {
            List<string> lowerNames = this._node.Clone().ToList();
            lowerNames.AddRange(nodeNames);

            return new Adress(lowerNames, this.Separator);
        }

        public IAdress GetLower(string nodeName)
            => this.GetLower(new List<string> { nodeName });

        public bool Equals(IAdress otherAdress)
            => this.CompareTo(otherAdress) == 0;

        public bool Equals(string otherAdress)
        {
            IAdress other = null;
            try
            {
                other = new Adress(otherAdress);
            }
            catch (ArgumentException)
            {
                return false;
            }

            return this.Equals(other);
        }

        public int CompareTo(IAdress other)
        {
            int compareByDeep = this.Deep - other.Deep;
            if (compareByDeep != 0)
            {
                return compareByDeep;
            }

            int nodeLength = this.Node.Count;
            for (int i = 0; i < nodeLength; i++)
            {
                int compareByString = this.Node[i].CompareTo(other.Node[i]);
                if (compareByString != 0)
                {
                    return compareByString;
                }
            }

            return 0;
        }
    }
}