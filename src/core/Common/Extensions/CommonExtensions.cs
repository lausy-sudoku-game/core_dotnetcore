using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LausySudokuGame.Core.Common.Extensions
{
    public static class CommonExtensions
    {
        /// <summary>
        /// Copies list without coping list items
        /// </summary>
        public static IList<T> Copy<T>(this IList<T> list)
            => list?.Select(item => item)?.ToList();

        /// <summary>
        /// Clones list with cloning list items
        /// </summary>
        public static IList<T> Clone<T>(this IList<T> list) where T: ICloneable
            => list?.Select(item => (T)item?.Clone())?.ToList();

        /// <summary>
        /// Removes last element of list
        /// </summary>
        /// <exception cref="System.ArgumentOutOfRangeException">throws if list is empty</exception>
        public static void RemoveLast<T>(this IList<T> list)
            => list.RemoveAt(list.Count - 1);

        /// <summary>
        /// Remove count last elemets of list
        /// </summary>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// throws if list length is less then count or count is negative
        /// </exception>
        public static void RemoveCount<T>(this IList<T> list, int count)
        {
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(count),
                    count,
                    "Can't remove negative count of elemets from collection"
                );
            }

            for (int i = 0; i < count; i ++)
            {
                list.RemoveLast();
            }
        }

        public static string ReplaceOnStringWithCode(this string source, string replaceSymbols, string replaceFormat)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < source.Length; i++)
            {
                char current = source[i];
                if (!replaceSymbols.Contains(current))
                {
                    result.Append(current);
                    continue;
                }

                string replaceValue = String.Format(replaceFormat, (int)current);
                result.Append(replaceValue);
            }

            return result.ToString();
        }
    }
}