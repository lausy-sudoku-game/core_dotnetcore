using System;
using System.Collections.Generic;

using LausySudokuGame.Core.Common.Tree;

namespace LausySudokuGame.Core.Model
{
    /// <summary>
    /// Kind of sudoku block
    /// </summary>
    public interface IBlockSignature
    {
        /// <summary>
        /// Block's type
        /// </summary>
        /// <remarks>
        /// defines rule of the block
        /// </remarks>
        /// <value>unique</value>
        IAdress Type { get; }

        /// <summary>
        /// Block's rule additional data
        /// </summary>
        /// <remarks>
        /// defines additional data for current rule if any required
        /// </remarks>
        /// <value>9</value>
        IList<string> Data { get; }

        /// <summary>
        /// Combined block's type and data: blok's singature
        /// </summary>
        /// <value>unique:9</value>
        string Full { get; }

        /// <summary>
        /// String separating <see cref="Type" /> and <see cref="Data" />
        /// in <see cref="Full" /> field
        /// </summary>
        string Separator { get; }
    }
}