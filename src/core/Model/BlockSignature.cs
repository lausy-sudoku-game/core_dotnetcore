using System;
using System.Collections.Generic;
using System.Linq;

using LausySudokuGame.Core.Common.Tree;

namespace LausySudokuGame.Core.Model
{
    public class BlockSignature : IBlockSignature
    {
        public const string DefaultSeparator = ":";

        public IAdress Type { get; private set; }
        public IList<string> Data { get; private set; }
        public string Full { get; private set; }
        public string Separator { get; private set; }

        public BlockSignature(string signature, string separator = DefaultSeparator)
        {
            this.Full = signature;
            this.Separator = separator;
            this.ParseSignature();
        }

        protected internal void ParseSignature()
        {
            List<string> splited = this.Full.Split(this.Separator, 2).ToList();
            string typeString = splited[0];
            string dataString = splited[1];

            this.Data = ParseDataString(dataString);
            this.Type = ParseTypeString(typeString);
        }

        protected internal static List<string> ParseDataString(string dataString)
            => dataString.Split().ToList();

        protected internal static IAdress ParseTypeString(string typeString)
            => new Adress(typeString);
    }
}