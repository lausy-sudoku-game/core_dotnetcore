using System;
using System.Collections.Generic;

namespace LausySudokuGame.Core.Model
{
    /// <summary>
    /// Represents sudoku information class
    /// </summary>
    public interface ISudoku : ICloneable
    {
        /// <summary>
        /// List of sudoku cells
        /// </summary>
        List<ICell> Cells { get; }

        /// <summary>
        /// List of sudoku blocks
        /// </summary>
        List<IBlock> Blocks { get; }
    }
}