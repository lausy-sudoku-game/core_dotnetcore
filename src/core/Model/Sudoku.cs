using System;
using System.Collections.Generic;
using System.Linq;

using LausySudokuGame.Core.Common.Extensions;

namespace LausySudokuGame.Core.Model
{
    public class Sudoku : ISudoku
    {
        public List<ICell> Cells { get; private set; }
        public List<IBlock> Blocks { get; private set; }

        public Sudoku(List<ICell> cells, List<IBlock> blocks)
        {
            this.Cells = (List<ICell>)cells.Clone();
            this.Blocks = (List<IBlock>)blocks.Clone();
        }

        public object Clone()
            => new Sudoku(this.Cells, this.Blocks);
    }
}