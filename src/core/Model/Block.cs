using System;
using System.Collections.Generic;
using System.Linq;

using LausySudokuGame.Core.Common.Extensions;

namespace LausySudokuGame.Core.Model
{
    public abstract class Block : IBlock
    {
        public const string DefaultSygnature = "Classic";

        public IBlockSignature Signature { get; private set; }

        public int Id { get; private set; }

        public IList<ICell> Cells { get; private set; }

        protected internal Block(int id, IList<ICell> cells, string signature = DefaultSygnature)
        {
            this.Id = id;
            this.Cells = cells.Copy();
            this.Signature = new BlockSignature(signature);
        }

        /// <summary>
        /// Clones block, without clonning cells;
        /// </summary>
        /// <seealso cref="ICloneable.Clone()" />
        public abstract object Clone();

        public abstract bool IsCorrect();

        protected internal List<KeyValuePair<bool, int>> GetValues()
            => this.Cells.ToList()
                .ConvertAll(cell => new KeyValuePair<bool, int>(cell.HasValue, cell.Value));

        protected internal List<int> GetValuesRaw(int noValue = 0)
            => this.Cells.ToList()
                .ConvertAll(cell => cell.HasValue ? cell.Value : noValue);

        protected internal List<int> GetIds()
            => this.Cells.ToList()
                .ConvertAll(cell => cell.Id);
    }
}