using System;

namespace LausySudokuGame.Core.Model
{
    /// <summary>
    /// Cell of sudoku
    /// </summary>
    public interface ICell : ICloneable
    {
        /// <summary>
        /// Cell's id
        /// </summary>
        /// <remarks>
        /// This field cannot be changed
        /// </remarks>
        int Id { get; }

        /// <summary>
        /// Indicates if cell's value can be changed.
        /// </summary>
        /// <remarks>
        /// This field cannot be changed
        /// </remarks>
        bool IsModified { get; }

        /// <summary>
        /// Indicates if cell has any value
        /// </summary>
        /// <remarks>
        /// Field can be changed through
        /// <see cref="Clear()" />, <see cref="Set(int)" />, <see cref="Change(bool, int)" />
        /// </remarks>
        bool HasValue { get; }

        /// <summary>
        /// Contains if cells has any value
        /// </summary>
        /// <remarks>
        /// <para>If <see cref="HasValue" /> is <c>false</c>, when this field must be ignored</para>
        /// <para>Field can be changed through
        /// <see cref="Clear()" />, <see cref="Set(int)" />, <see cref="Change(bool, int)" /></para>
        /// </remarks>
        int Value { get; }

        /// <summary>
        /// Sets value or clears it.
        /// </summary>
        /// <remarks>
        /// <para>If <paramref name="hasValue" /> is <c>false</c>, <paramref name="value" /> is ignored</para>
        /// <para>Can be applied only if <see cref="IsModified" /> is <c>true</c></para>
        /// </remarks>
        /// <param name="hasValue">new value for <see cref="HasValue" /></param>
        /// <param name="value">new value for <see cref="Value" /></param>
        /// <seealso cref="Set(int)" />
        /// <seealso cref="Clear()" />
        void Change(bool hasValue, int value);

        /// <summary>
        /// Sets <see cref="Value" /> to specified value and <see cref="HasValue" /> to <c>true</c>
        /// </summary>
        /// <remarks>
        /// Can be applied only if <see cref="IsModified" /> is <c>true</c>
        /// </remarks>
        /// <param name="value">new value</param>
        /// <seealso cref="Change(bool,int)" />
        void Set(int value);

        /// <summary>
        /// Sets <see cref="HasValue" /> to <c>false</c>
        /// </summary>
        /// <remarks>
        /// Can be applied only if <see cref="IsModified" /> is <c>true</c>
        /// </remarks>
        /// <seealso cref="Change(bool,int)" />
        void Clear();
    }
}