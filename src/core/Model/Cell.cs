using System;

namespace LausySudokuGame.Core.Model
{
    public class Cell : ICell
    {
        public const int DefaultValue = 0;
        public const bool DefaultHasValue = false;
        public const bool DefaultIsModifiedValue = true;

        public int Id { get; private set; }
        public bool IsModified { get; private set; }
        public bool HasValue { get; private set; }
        public int Value { get; private set; }

        public Cell(
            int id,
            int value = DefaultValue,
            bool hasValue = DefaultHasValue,
            bool isModified = DefaultIsModifiedValue)
        {
            this.Id = id;
            this.IsModified = isModified;
            this.HasValue = hasValue;
            this.Value = value;
        }

        public void Change(bool hasValue, int value)
        {
            if (this.IsModified)
            {
                this.HasValue = hasValue;
                this.Value = value;
            }
        }

        public void Set(int value)
            => Change(hasValue: true, value);

        public void Clear()
            => Change(hasValue: false, DefaultValue);

        public object Clone()
            => new Cell(this.Id, this.Value, this.HasValue, this.IsModified);
    }
}