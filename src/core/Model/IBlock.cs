using System;
using System.Collections.Generic;

namespace LausySudokuGame.Core.Model
{
    /// <summary>
    /// Block of sudoku cells
    /// </summary>
    public interface IBlock : ICloneable
    {
        /// <summary>
        /// Id of block
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Block's signature
        /// </summary>
        /// <value>Classic::9</value>
        IBlockSignature Signature { get; }

        /// <summary>
        /// Returns block of cells;
        /// </summary>
        IList<ICell> Cells { get; }

        /// <summary>
        /// Returns true if block cells fit the block rule
        /// </summary>
        bool IsCorrect();
    }
}