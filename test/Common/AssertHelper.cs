using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using LausySudokuGame.Core.Model;
using LausySudokuGame.Core.Common.Tree;
using LausySudokuGame.Core.Test.Data.Unit.Model;

namespace LausySudokuGame.Core.Test.Common
{
    public static class AssertHelper
    {
        public static void AssertNotEmptyOrNull<T>(IEnumerable<T> list)
            => Assert.True(list?.Any() ?? false);

        public static void AssertCell(CellData expected, ICell actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.IsModified, actual.IsModified);
            Assert.Equal(expected.HasValue, actual.HasValue);

            if (expected.HasValue)
            {
                Assert.Equal(expected.Value, actual.Value);
            }
        }

        public static void AssertAdress(
            IAdress actual,
            IList<string> nodesExpected,
            string separatorExpected)
        {
            Assert.Equal(nodesExpected, actual.Node);
            Assert.Equal(separatorExpected, actual.Separator);

            Assert.Equal(String.Join(separatorExpected, nodesExpected), actual.Full);
            Assert.Equal(nodesExpected.Count, actual.Deep);
            Assert.Equal(nodesExpected.Count == 1, actual.IsRoot);
        }
    }
}