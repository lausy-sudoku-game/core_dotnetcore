using System.Collections.Generic;
using System.Linq;
using Xunit;

using LausySudokuGame.Core.Test.Common;

namespace LausySudokuGame.Core.Test.Data.General
{
    public class TheoryDataSet<T> : TheoryData<T>
    {
        public TheoryDataSet(IEnumerable<T> list)
        {
            AssertHelper.AssertNotEmptyOrNull(list);
            this.AddData(list);
        }

        private void AddData(IEnumerable<T> list)
        {
            foreach (var item in list)
            {
                this.Add(item);
            }
        }
    }
}