using System.Collections.Generic;
using Xunit;

using LausySudokuGame.Core.Test.Common;

namespace LausySudokuGame.Core.Test.Data.General
{
    public class TheoryDataSet<T1, T2> : TheoryData<T1, T2>
    {
        public TheoryDataSet(IEnumerable<T1> list1, IEnumerable<T2> list2)
        {
            AssertHelper.AssertNotEmptyOrNull(list1);
            AssertHelper.AssertNotEmptyOrNull(list2);

            this.AddData(list1, list2);
        }

        public TheoryDataSet(IDictionary<T1, List<T2>> list)
        {
            AssertHelper.AssertNotEmptyOrNull(list);

            this.AddData(list);
        }

        public TheoryDataSet(IEnumerable<KeyValuePair<T1, T2>> list)
        {
            AssertHelper.AssertNotEmptyOrNull(list);

            this.AddData(list);
        }

        private void AddData(IEnumerable<T1> list1, IEnumerable<T2> list2)
        {
            foreach (var itemType1 in list1)
            {
                foreach (var itemType2 in list2)
                {
                    this.Add(itemType1, itemType2);
                }
            }
        }

        private void AddData(IDictionary<T1, List<T2>> list)
        {
            foreach (var pair in list)
            {
                foreach (var valueItem in pair.Value)
                {
                    this.Add(pair.Key, valueItem);
                }
            }
        }

        private void AddData(IEnumerable<KeyValuePair<T1, T2>> list)
        {
            foreach (var pair in list)
            {
                this.Add(pair.Key, pair.Value);
            }
        }
    }
}