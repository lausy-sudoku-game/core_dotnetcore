using System.Collections.Generic;
using System.Linq;
using Xunit;

using LausySudokuGame.Core.Test.Common;

namespace LausySudokuGame.Core.Test.Data.General
{
    public class TheoryDataSet<T1, T2, T3> : TheoryData<T1, T2, T3>
    {
        public TheoryDataSet(
            IEnumerable<T1> list1,
            IEnumerable<T2> list2,
            IEnumerable<T3> list3)
        {
            AssertHelper.AssertNotEmptyOrNull(list1);
            AssertHelper.AssertNotEmptyOrNull(list2);
            AssertHelper.AssertNotEmptyOrNull(list3);

            this.AddData(list1, list2, list3);
        }

        private void AddData(
            IEnumerable<T1> list1,
            IEnumerable<T2> list2,
            IEnumerable<T3> list3)
        {
            foreach (var itemType1 in list1)
            {
                foreach (var itemType2 in list2)
                {
                    foreach (var itemType3 in list3)
                    {
                        this.Add(itemType1, itemType2, itemType3);
                    }
                }
            }
        }
    }
}