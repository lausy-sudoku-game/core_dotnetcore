namespace LausySudokuGame.Core.Test.Data.Unit.Model
{
    public struct CellData
    {
        public bool IsModified;
        public bool HasValue;
        public int Id;
        public int Value;
    }
}