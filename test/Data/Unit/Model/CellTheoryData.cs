using System.Collections.Generic;

namespace LausySudokuGame.Core.Test.Data.Unit.Model
{
    public class CellTheoryData : List<CellData>
    {
        public CellTheoryData(List<int> idList, List<int> valueList, List<bool> hasValueList, List<bool> isModifiedList)
            => this.AddSet(idList, valueList, hasValueList, isModifiedList);

        private void AddSet(List<int> idList, List<int> valueList, List<bool> hasValueList, List<bool> isModifiedList)
        {
            foreach (int id in idList)
            {
                foreach (int value in valueList)
                {
                    AddSetWithBooleanValues(id, value, hasValueList, isModifiedList);
                }
            }
        }

        private void AddSetWithBooleanValues(int id, int value, List<bool> hasValueList, List<bool> isModifiedList)
        {
            foreach (bool hasValue in hasValueList)
            {
                foreach (bool isModified in isModifiedList)
                {
                    this.Add(new CellData { Id = id, Value = value, HasValue = hasValue, IsModified = isModified });
                }
            }
        }
    }
}