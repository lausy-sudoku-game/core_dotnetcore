using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using LausySudokuGame.Core.Common.Extensions;
using LausySudokuGame.Core.Common.Tree;
using LausySudokuGame.Core.Test.Common;
using LausySudokuGame.Core.Test.Data.General;

namespace LausySudokuGame.Core.Test.Unit.Common.Tree
{
    public class AdressTest
    {
        #region TestData

        public const string NotSeparatorFormat = "NotA{0}Separator";
        public const int ConstructorNode_MaxLength = 4;
        public const int ConstructorString_MaxLength = 3;
        public const int Upper_ArgumentOutOfRange_MaxLength = 3;
        public const int Upper_ArgumentOutOfRange_MaxDeep = 4;
        public const int Upper_MaxLength = 4;
        public const int Upper_MaxDeep = 3;
        public const int UpperOne_MaxLength = 3;

        public readonly static List<string> CorrectSeparators
            = new List<string> { ".", "..", "Andy", "\\", "🤔", " " };

        public readonly static List<string> CorrectNodesName
            = new List<string> { String.Empty, "ANDY", "adstra", " ", ".", "🤔", "AFst0" };

        public readonly static List<List<string>> OutOfRangeNodes
            = new List<List<string>> { null, new List<string>() };

        public readonly static List<string> OutOfRangeStrings
            = new List<string> { null };

        public static TheoryDataSet<List<string>, string> ConstructorNode_NodeOutOfRangeData
            = new TheoryDataSet<List<string>, string>(OutOfRangeNodes, CorrectSeparators);

        public static TheoryDataSet<List<string>, string> ConstructorNode_NodeArgumentData
            = new TheoryDataSet<List<string>, string>(
                new Dictionary<List<string>, List<string>>
                {
                    {
                        new List<string> { String.Join(String.Empty, CorrectSeparators) },
                        CorrectSeparators
                    },
                    {
                        CorrectSeparators,
                        CorrectSeparators
                    }
                });

        public static TheoryDataSet<List<string>, string> ConstructorNodeData
            = new TheoryDataSet<List<string>, string>(
                GenerateNodeLists(CorrectNodesName, ConstructorNode_MaxLength),
                CorrectSeparators);

        public static TheoryDataSet<string, string> ConstructorString_StringOutOfRangeData
            = new TheoryDataSet<string, string>(OutOfRangeStrings, CorrectSeparators);

        public static TheoryDataSet<string, string> ConstructorStringData
            = new TheoryDataSet<string, string>(
                GenerateStringConstructorData(CorrectNodesName, CorrectSeparators, ConstructorString_MaxLength));

        public static TheoryDataSet<List<string>, string, int> Upper_ArgumentOutOfRangeData
            = new TheoryDataSet<List<string>, string, int>(
                GenerateNodeLists(CorrectNodesName, Upper_ArgumentOutOfRange_MaxLength),
                CorrectSeparators,
                Enumerable.Range(0, Upper_ArgumentOutOfRange_MaxDeep));

        public static TheoryDataSet<List<string>, string, int> UpperData
            = new TheoryDataSet<List<string>, string, int>(
                GenerateNodeLists(CorrectNodesName, Upper_MaxLength, Upper_MaxDeep + 1),
                CorrectSeparators,
                Enumerable.Range(0, Upper_MaxDeep));

        public static TheoryDataSet<List<string>, string> UpperOne_ArgumentOutOfRangeData
            = new TheoryDataSet<List<string>, string>(
                GenerateNodeLists(CorrectNodesName, maxLength: 1), CorrectSeparators);

        public static TheoryDataSet<List<string>, string> UpperOneData
            = new TheoryDataSet<List<string>, string>(
                GenerateNodeLists(CorrectNodesName, UpperOne_MaxLength, minLength: 2),
                CorrectSeparators);

        #endregion

        #region HelpMethods

        private static List<string> CodeToNodeList(int code, IList<string> nodeNames, int length)
        {
            List<string> result = new List<string> { };
            int current = code;
            int basis = nodeNames.Count;

            for (int i = 0; i < length; i++, current = current / basis)
            {
                result.Add(nodeNames[current % basis]);
            }

            return result;
        }

        private static List<List<string>> GenerateNodeListsSameLength(IList<string> nodeNames, int length)
        {
            List<List<string>> result = new List<List<string>> { };
            int maxCode = (int)Math.Pow(nodeNames.Count, length);

            for (int code = 0; code < maxCode; code++)
            {
                result.Add(CodeToNodeList(code, nodeNames, length));
            }

            return result;
        }

        private static List<List<string>> GenerateNodeLists(List<string> nodeNames, int maxLength, int minLength = 1)
        {
            List<List<string>> result = new List<List<string>> { };

            for (int i = minLength; i <= maxLength; i++)
            {
                result.AddRange(GenerateNodeListsSameLength(nodeNames, i));
            }

            return result;
        }

        private static List<KeyValuePair<string, string>> GenerateStringConstructorData(
            List<string> nodeNames, List<string> separators, int maxLength, int minLength = 1)
        {
            List<List<string>> generatedNodes = GenerateNodeLists(nodeNames, maxLength, minLength);
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

            for (int i = 0; i < separators.Count; i++)
            {
                List<string> joinedNodes = generatedNodes.ConvertAll(
                    (nodeList) => String.Join(
                        separators[i],
                        GetCleanedNodeNamesFromSeparator(nodeList, separators[i]))
                );
                result.AddRange(joinedNodes
                    .ConvertAll(stringAdress => new KeyValuePair<string, string>(stringAdress, separators[i]))
                );
            }

            return result;
        }

        private static List<string> GetCleanedNodeNamesFromSeparator(List<string> nodes, string separator)
            => nodes.ConvertAll(node => node.ReplaceOnStringWithCode(separator, NotSeparatorFormat));

        #endregion

        #region TestMethods

        [Theory]
        [MemberData(nameof(ConstructorNode_NodeOutOfRangeData))]
        public void ConstructorNode_NodeOutOfRange(List<string> node, string separator)
            => Assert.Throws<ArgumentOutOfRangeException>(() => new Adress(node, separator));

        [Theory]
        [MemberData(nameof(ConstructorNode_NodeArgumentData))]
        public static void ConstructorNode_NodeArgument(List<string> node, string separator)
            => Assert.Throws<ArgumentException>(() => new Adress(node, separator));

        [Theory]
        [MemberData(nameof(ConstructorNodeData))]
        public void ConstructorNode(List<string> nodes, string separator)
        {
            List<string> excpectedNodes = GetCleanedNodeNamesFromSeparator(nodes, separator);
            Adress adress = new Adress(
                excpectedNodes,
                separator);

            AssertHelper.AssertAdress(
                actual: adress,
                nodesExpected: excpectedNodes,
                separatorExpected: separator
            );
        }

        [Theory]
        [MemberData(nameof(ConstructorString_StringOutOfRangeData))]
        public void ConstructorString_StringOutOfRange(string stringAdress, string separator)
            => Assert.Throws<ArgumentOutOfRangeException>(() => new Adress(stringAdress, separator));

        [Theory]
        [MemberData(nameof(ConstructorStringData))]
        public void ConstructorString(string stringAdress, string separator)
        {
            List<string> nodesExcepcted = stringAdress.Split(separator).ToList();

            Adress adress = new Adress(stringAdress, separator);

            AssertHelper.AssertAdress(
                actual: adress,
                nodesExpected: nodesExcepcted,
                separatorExpected: separator
            );
        }

        [Theory]
        [MemberData(nameof(Upper_ArgumentOutOfRangeData))]
        public void Upper_ArgumentOutOfRange(List<string> nodes, string separator, int deepFactor)
        {
            nodes = GetCleanedNodeNamesFromSeparator(nodes, separator);
            int triggerDeep = nodes.Count;
            int deep = triggerDeep + deepFactor;
            Adress adress = new Adress(nodes, separator);

            Assert.Throws<ArgumentOutOfRangeException>(
                () => adress.GetUpper(deep));
        }

        [Theory]
        [MemberData(nameof(UpperData))]
        public void Upper(List<string> nodes, string separator, int deep)
        {
            nodes = GetCleanedNodeNamesFromSeparator(nodes, separator);
            IList<string> nodesExpected = nodes.Clone();
            nodesExpected.RemoveCount(deep);
            Adress adress = new Adress(nodes, separator);

            IAdress actual = adress.GetUpper(deep);

            AssertHelper.AssertAdress(actual, nodesExpected, separatorExpected: separator);
        }

        [Theory]
        [MemberData(nameof(UpperOne_ArgumentOutOfRangeData))]
        public void UpperOne_ArgumentOutOfRange(List<string> nodes, string separator)
        {
            nodes = GetCleanedNodeNamesFromSeparator(nodes, separator);
            Adress adress = new Adress(nodes, separator);

            Assert.Throws<ArgumentOutOfRangeException>(
                () => adress.GetUpper());
        }

        [Theory]
        [MemberData(nameof(UpperOneData))]
        public void UpperOne(List<string> nodes, string separator)
        {
            nodes = GetCleanedNodeNamesFromSeparator(nodes, separator);
            IList<string> nodesExpected = nodes.Clone();
            nodesExpected.RemoveLast();
            Adress adress = new Adress(nodes, separator);

            IAdress actual = adress.GetUpper();

            AssertHelper.AssertAdress(actual, nodesExpected, separatorExpected: separator);
        }

        #endregion
    }
}
