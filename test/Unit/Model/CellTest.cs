using System.Collections.Generic;
using Xunit;

using LausySudokuGame.Core.Model;
using LausySudokuGame.Core.Test.Common;
using LausySudokuGame.Core.Test.Data.General;
using LausySudokuGame.Core.Test.Data.Unit.Model;

namespace LausySudokuGame.Core.Test.Unit.Model
{
    public class CellTest
    {
        #region TestData

        public readonly static List<int> CorrectIds
            = new List<int> { 81, 144, 256, 400, System.Int32.MaxValue, System.Int32.MinValue, 0, 1, -1, 2, -2 };

        public readonly static List<int> CorrectValues
            = new List<int> { 81, 144, 256, 400, System.Int32.MaxValue, System.Int32.MinValue, 0, 1, -1, 2, -2 };

        public readonly static List<bool> CorrectHasValues
            = new List<bool> { true, false };

        public readonly static List<bool> CorrectIsModifiedValues
            = new List<bool> { true, false };

        public static CellTheoryData CorrectCells
            = new CellTheoryData(CorrectIds, CorrectValues, CorrectHasValues, CorrectIsModifiedValues);

        public static TheoryDataSet<CellData> CorrectCellsTheoryData
            = new TheoryDataSet<CellData>(CorrectCells);

        public static TheoryDataSet<CellData> ConstructorTestData
            = CorrectCellsTheoryData;

        public static TheoryDataSet<CellData> ClearTestData
            = CorrectCellsTheoryData;

        public static TheoryDataSet<CellData, int> SetTestData
            = new TheoryDataSet<CellData, int>(CorrectCells, CorrectValues);

        public static TheoryDataSet<CellData, int, bool> ChangeTestData
            = new TheoryDataSet<CellData, int, bool>(CorrectCells, CorrectValues, CorrectHasValues);

        #endregion

        #region TestMethods

        [Theory]
        [MemberData(nameof(ConstructorTestData))]
        public void Constructor(CellData data)
        {
            Cell actual = new Cell(data.Id, data.Value, data.HasValue, data.IsModified);
            CellData expected = data;

            AssertHelper.AssertCell(expected, actual);
        }

        [Theory]
        [MemberData(nameof(ClearTestData))]
        public void Clear(CellData data)
        {
            Cell actual = new Cell(data.Id, data.Value, data.HasValue, data.IsModified);
            CellData expected = new CellData
            {
                Id = data.Id,
                Value = data.Value,
                HasValue = data.IsModified ? false : data.HasValue,
                IsModified = data.IsModified
            };

            actual.Clear();

            AssertHelper.AssertCell(expected, actual);
        }

        [Theory]
        [MemberData(nameof(SetTestData))]
        public void Set(CellData data, int newValue)
        {
            Cell actual = new Cell(data.Id, data.Value, data.HasValue, data.IsModified);
            CellData expected = new CellData {
                Id = data.Id,
                IsModified = data.IsModified,
                HasValue = data.IsModified ? true : data.HasValue,
                Value = data.IsModified ? newValue : data.Value
            };

            actual.Set(newValue);

            AssertHelper.AssertCell(expected, actual);
        }

        [Theory]
        [MemberData(nameof(ChangeTestData))]
        public void Change(CellData data, int newValue, bool newHasValue)
        {
            Cell actual = new Cell(data.Id, data.Value, data.HasValue, data.IsModified);
            CellData expected = new CellData {
                Id = data.Id,
                IsModified = data.IsModified,
                HasValue = data.IsModified ? newHasValue : data.HasValue,
                Value = data.IsModified ? newValue : data.Value
            };

            actual.Change(newHasValue, newValue);

            AssertHelper.AssertCell(expected, actual);
        }

        #endregion
    }
}